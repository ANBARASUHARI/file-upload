from django.db import models

# Create your models here.

class User(models.Model):

    first_name = models.CharField(max_length=100)
    father_name = models.CharField(max_length=100)
    kyc = models.FileField(upload_to='documents/')