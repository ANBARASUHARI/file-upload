from django.shortcuts import render, redirect
from .models import User
from .forms import UserForm

# Create your views here.

def userinfo(request):
    print(request.method)
    form = UserForm()
    if request.method == "POST":
        form = UserForm(request.POST, request.FILES)
        
        if form.is_valid():
            form.save()
            return redirect('/')

    else:
        form = UserForm()
        data = {"form": form}
    return render(request, "userinfo.html", context=data)